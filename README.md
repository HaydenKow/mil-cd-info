# __Mil-CD Creation Guide__    <!-- omit in toc -->
*Written by HaydenKow/NeoDC 2019*

## ___Table of Contents___ <!-- omit in toc -->
- [Tools Needed](#tools-needed)
- [Files Needed](#files-needed)
- [Basic Setup](#basic-setup)
- [Playlist Editing](#playlist-editing)
- [Image Editing](#image-editing)
  - [__Terms and Filenames__](#terms-and-filenames)
  - [PVR Creation](#pvr-creation)
- [Video Creation](#video-creation)
  - [__FFmpeg Commands__](#ffmpeg-commands)
  - [__Windows 98 Virtual Machine Usage__](#windows-98-virtual-machine-usage)
- [Music Creation for Audio Mil-CD](#music-creation-for-audio-mil-cd)
- [Final Steps](#final-steps)
  - [__Iso Creation__](#iso-creation)
  - [Choose a Scenario: Mil-CD Video Player OR Full Mil-CD Usage](#choose-a-scenario-mil-cd-video-player-or-full-mil-cd-usage)
  - [__Mil-CD Video Player__](#mil-cd-video-player)
    - [_Audio Tracks & Video tracks under Mil-CD Menu__](#audio-tracks--video-tracks-under-mil-cd-menu)
  - [__Full Mil-CD Usage__](#full-mil-cd-usage)
    - [_Audio Tracks & Video tracks under Mil-CD Menu__](#audio-tracks--video-tracks-under-mil-cd-menu-1)
- [Missing (To Be Added)](#missing-to-be-added)
- [Changes](#changes)

# Tools Needed
- IsoBuster (4.3 free edition is fine)
- Windows98 with Dreamcast Movie Creator
- FFMPEG
- NeoDC Basic Mil-CD Tools
  -  Custom Mkisofs (may change in future)
  -  cdi4dc/mds4dc
  -  lbacalc
  -  dummy
  -  Modified IP.BIN (bootfile.bin)
-  PVR Creation Tools
   -  pvrconv
   -  Image Editor (supplied by you)

# Files Needed
- Mil-CD Template files
- Images
- Music


# Basic Setup
### _Basic Setup of Files and Tools_ <!-- omit in toc -->
1. Unzip the Tools into a folder
2. make a subdir called "``data``"
   1. unzip the data template into this folder
3. make a subdir called "``music-raw``"
4. Refer to Fig A.
5. At this point most of the base setup is done except for [Playlist Editing](#playlist-editing), [Image Editing](#image-editing), and [Video Creation](#video-creation)
   1. Refer to the Section that is prevalent or go through sequentially'

Fig A.:

![fig a](img/WY5GGeU.png "Fig A.")

# Playlist Editing
### _This Section is about the Playlist file that drives the Mil-CD Menu_ <!-- omit in toc -->
1. in the "``data``" subdir go to the "``SOFDEC``" folder and find the file called "``MILSFD.PLY``"
2. Open this file in a text editor
```
;MIL CD SAMPLE DISC
;1999-10-18

[MilcdSofdecPlayer]
milsfd.ply

[TotalTrack]
1

[VideoTrack]
;Filename    SizeXY  Bitrate Time
vidtrk01.sfd 352 240 5.4777 146010

[DiscTitle]
discttl.pvr

[TrackTitle]
trkttl01.pvr

[JacketPicture]
jackpic.pvr

[SofdecLogo]
sfdlogo.pvr

[RecordLogo]
reclogo.pvr

;end of file
```
## Explanation of this file: <!-- omit in toc -->
- lines beginning with "``;``" are comments and ignored
- the first section "``[MilcdSofdecPlayer]``" should be untouched
- "``[TotalTrack]``" should match the following section with how many video tracks, up to 65 have been tested succesfully.
- "``[VideoTrack]``" is list of video and associated info with one file per line
  - The values all come from Dreamcast Movie Creator ([Video Creation](#video-creation))
- The following sections define images for the Disc, Track and other logos.
  - If you have many videos and want the Track Image to update with each video you can add one texture per line under "``[TrackTitle]``" eg.
  - Each texture needs the next sequential Global Index (more on this in image editing)
```
[TrackTitle]
trkttl01.pvr
trkttl02.pvr
trkttl03.pvr
```

# Image Editing
### _This Section is focused on creating custom images for your disc_ <!-- omit in toc -->
## ___Transforming this___ <!-- omit in toc -->
![SC5 Original Menu](img/xzQfNC6.png "SC5 Original")
## ___Into This___ <!-- omit in toc -->
![Rick Roll Menu](img/JvODLDR.png "Rick Roll Menu")

## __Terms and Filenames__
- discttl.pvr - Disc Title: "NEO" in the above, and the Stylized 5 Logo in the previous.
- trkttl01.pvr - Track Title: "Never Gonna Give you Up", and "MEXICAN FLYER ORIGINAL"
  - could be many of these, 1 per track if you choose.
- jackpic.pvr - Jacket Picture: "You Just Got Rick Rolled", and the Female Face Picture.

## PVR Creation
- Images with alpha get made in standard way in your own image editor
  - If you want to use an Alpha mask, black is visible, white is invisible and can gradient through those values
  - Images need to be saved as a normal 24bit BMP, no exceptions.
  - Each of these are 256x256
  - DISCTTL.pvr will be stretched, but i havent done the math to figure out the scale ratio.
- After you create your images to your liking, these are example commands that will transform them into correct PVRs.
```
pvrconv.exe -5 -t -gi 1001 JACKPIC.bmp
pvrconv.exe -4 -t -gi 1000 TRKTTL01.bmp TRKTTL01_a.bmp
pvrconv.exe -4 -t -gi 1003 DISCTTL.bmp DISCTTL_a.bmp
```
- If you are unsure if you have done things correctly, refer to example images or open with PVieweR.exe or your favorite pvr viewer.
- Once converted these files, go in the "``SOFDEC``" subdir, and can replace the originals.

# Video Creation
### _This Section is about using Dreamcast Movie Creator to encode SFDs_ <!-- omit in toc -->
### __THIS REQUIRES WINDOWS 98, NO EXCEPTIONS, NO WORKAROUNDS__ <!-- omit in toc -->

1. Finding a video and getting into the correct format
    - You are able to use just about any file, because we will be using FFMPEG to convert it
    -
## __FFmpeg Commands__

__Video__
- 2 step high quality, re-encode from any movie to correct format and size.
- replace MOVIE_FILE with the movie file you want to use.
- Run first command, then second.
```
ffmpeg -y -i 'MOVIE_FILE' -b:v 5000K -maxrate 5000K -minrate 5000K -bufsize 5000K -vf scale=352:240,setsar=200/219,setdar=880/657 -an -c:v libxvid -mbd rd -flags +mv4+aic -trellis 2 -cmp 2 -subcmp 2 -vtag xvid -pass 1 -f avi NUL

ffmpeg -y -i 'MOVIE_FILE' -b:v 5000K -maxrate 5000K -minrate 5000K -bufsize 5000K -vf scale=352:240,setsar=200/219,setdar=880/657 -c:a libmp3lame -ar 44100 -b:a 192k -ac 2 -c:v libxvid -mbd rd -flags +mv4+aic -trellis 2 -cmp 2 -subcmp 2 -vtag xvid -pass 2 encode.avi
```
__Audio__
```
ffmpeg -y -i 'MOVIE_FILE' -vn -acodec pcm_s16le -ar 44100 -ac 2 encode.wav
```
- These are the files we will be using under Windows98 to encode a correct SFD.

## __Windows 98 Virtual Machine Usage__
- Using the included win98 virtual machine, or your own correctly installed and setup machine
- Dreamcast Movie Creator must be installed, so must Divx or Xvid.
- Open the Dreamcast Movie Creator Software
![alt text](img/ioOoqBS.png "Logo Title Text 1")
- You can either setup a new project by selecting the "Video" option near the top and selecting your "``encode.avi``" file we created.
- The Defaults it gives will be fine, but if you want to adjust anything feel free.
 ![alt text](img/DjM3Mmq.png "Logo Title Text 1")
- __IMPORTANT__ If you hit "Start Encoding" this will fail!
- Steps
  1. Change Type to "Mpeg-1 Video .m1v"
     1. hit "Start Encoding"
  2. Uncheck "Use Identical Files"
  3. Select the "``encode.wav``" file for the audio portion
  4. Change Type to "Sofdec Audio .sfa"
     1. hit Start encoding
  5. Now almost there
  6. Change Type to "Sofdec Video .sfd"
  7. For Video browse to the .m1v file
  8. For Audio browse to the .sfa file
  9. Hit Start encoding, it should be fairly quick.

__This is your newly encoded video file__

Now to record the values from this tool for use in the Playlist file
- filename should be renamed to follow "``VIDTRKXX.SFD``" where XX is a 2 digit number, eg. 01
- SIZE X Y = 352 240, and is given in the main tool window
- Bitrate is found in the lower left log window under Total Data Rate, 3996 kbits/sec = 3.996
- Time is the value in the Sec field near the top with the period removed, eg. 212.637 = 212637
- the total line would be "``VIDTRK01.SFD 352 240 3.996 212637``"

# Music Creation for Audio Mil-CD
- gather your music
- for each track on the disc run this ffmpeg command
- ``ffmpeg -y-i INPUT_SONG.MP3 -acodec pcm_s16le -f s16le -ac 2 -ar 44100 trackX.raw``
  - "``INPUT_SONG.MP3``" is your input music and X goes up for each track.
- Each of new "``.raw``" files goes into the "``music-raw``" folder
- run "``lba_calc.bat``" and write down the number it gives you
- in the "``make_iso.bat``" (This EXACT one ONLY) edit the number after "0," to match eg.
  - ``mkisofs -C 0,43512``


# Final Steps
## Back to the SOFDEC subdir <!-- omit in toc -->
- Edit the "``MILDSFD.PLY``" file and add your new information under the "``[VideoTrack]``" section
- Move the video and any others into the SOFDEC Folder
- Move your newly created PVR images into the same SOFDEC Folder
- edit "``bootfile.bin``", it is simply an "``IP.BIN``", to change name
- edit the "``make_iso.bat``" and "``make__skinny_iso.bat``" files to change the volume name from CUSTOM_MILCD to whatever you'd like

##  __Iso Creation__

## __IMPORTANT Note (Your Mil-CD will not boot without this!)__
### _`CDPLUS/INFO.CDP`_  Must be located 75 sectors above session 2 LBA

For example, you're making a mil-cd with no cd tracks and only videos.

Likely you'll be at LBA 11702, which means your `CDPLUS/INFO.CDP` file __MUST__ be at LBA 11777

You can verify where your second session starts with iso buster by opening the cdi and clicking session 2 on the left.
![Showing LBA 11702 for Session2](img/session2.png "Showing LBA 11702 for Session2")

Easiest way to accomplish this is a 2 step process when making your mil-cd.
* Make it as normal with no padding on the data session.
* Use IsoBuster to check the LBA of your second session and the INFO.CDP file.
* In this example it is at LBA 11728, and we made an 11702 LBA CDI, notice how 11728-11702 = 26. 26 is not 75
* Do some simple math, in this case we need to move ahead 49 sectors. each sector is 2048 bytes so 49 * 2048 = 100,352
* Make a file exactly 100,352 bytes and call it `0.0` and put it in the `CDPLUS` folder
* remake your mil-cd cdi and verify again in IsoBuster. If is still is not where you need it to be, remake your `0.0` in less or more 2048 byte blocks until it matches.

### Before, LBA 11728:

![Showing LBA 11728 for INFO.CDP](img/lba_before.png "Showing LBA 11728 for INFO.CDP")

### After, LBA 11777:

![Showing LBA 11777 for INFO.CDP](img/lba_after.png "Showing LBA 11777 for INFO.CDP")

## Choose a Scenario: [Mil-CD Video Player](#Mil-CD-Video-Player) OR [Full Mil-CD Usage](#Full-Mil-CD-Usage)

## __Mil-CD Video Player__
### _Audio Tracks & Video tracks under Mil-CD Menu_
- This Variant has NO music tracks, but otherwise works fine
- Execute "``make_skinny_iso.bat``" to make a simple audio/data LBA 11702 iso
- Execute "``make_skinny_cdi.bat``" to transform that into a simple .cdi

## __Full Mil-CD Usage__
### _Audio Tracks & Video tracks under Mil-CD Menu_
- This Variant has music tracks and video tracks
- Ensure you have done this step [Music Creation for Audio Mil-CD](#music-creation-for-audio-mil-cd)
- Execute "``make_iso.bat``" to make a simple audio/data LBA 11702 iso
- Execute "``make_mds.bat``" to transform that into a simple .cdi

# Missing (To Be Added)
- SUB_INFO.XX information and editing
- INFO.CDP information and editing
- PICTURES subdir explanation and editing (MPEG1, JFIF)
- Theming the menu more completely
- Web browser info
# Changes
- v1: first public release